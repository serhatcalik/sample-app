![Solution](./solution.png)

I created a multistage Dockerfile, but the .NET runtime didn't work as expected, so I ended up using the SDK on the working environment too. I also created a Helm chart for the sample app and used Nginx ingress on my homelab. If needed, I can show them to you. For deployment, I used ArgoCD, as it's easier to handle Helm charts